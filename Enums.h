#ifndef ENUMS_H
#define ENUMS_H

enum menuKeys{
    upKey,
    downKey,
    enterKey,
    unknownKey
};

enum directions{
    up,
    right,
    down,
    left,
    error
};

enum actions
{
    unknown,
    step,
    fight,
    use,
    take,
    look,
    inventory,
    taunt,
    talk,
    exitGame,
    ACTIONS_COUNTER // must be on the end
};

enum fieldTypes
{
    normal=1,
    doors
};
#endif // ENUMS_H
