#include "Field.h"


Field::Field()
    :type(fieldTypes::normal)
    ,description("Nic specialnego")
{
    for(int i=0; i<4; ++i)
    {
        neighbors[i] = NULL;
    }
}

Field::Field(fieldTypes _type)
    :type(_type)
    ,description("Nic specialnego")
{
    for(int i=0; i<4; ++i)
    {
        neighbors[i] = NULL;
    }
}


Field::~Field()
{

}

void Field::setDescription(string description_text)
{
    description = description_text;
}

string Field::getDescription()
{
    return description;
}
