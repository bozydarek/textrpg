#ifndef FIELD_H
#define FIELD_H

#include <cstdlib>
#include <iostream>

#include "Enums.h"

using namespace std;

class Field
{
    public:
        Field();
        Field( fieldTypes _type );
        virtual ~Field();

        string  getDescription();
        void    setDescription(string description_text);

    protected:
        fieldTypes  type;

    private:
        Field*      neighbors[4];
        string      description;
};

#endif // FIELD_H
