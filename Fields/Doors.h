#ifndef DOORS_H
#define DOORS_H

#include "../Field.h"

class Location;

class Doors : public Field
{
    public:
        Doors();
        virtual ~Doors();
    protected:

    private:
        //Location*   nextLocation;
        Field*      next;
};

#endif // DOORS_H
