#include "Location.h"

#include <assert.h>
#include <cstdlib>
#include <fstream>
#include <iostream>

Location::Location():
    respawn(NULL)
{

}

Location::~Location()
{

}

Field* Location::respawn_position()
{
    assert( respawn != NULL ); // In this location respawn is not set yet
    return respawn;
}

void Location::setRespawn(Field* respawn_field)
{
    respawn = respawn_field;
}

void Location::setDescription(string description_text)
{
    description = description_text;
}

string Location::getDescription()
{
    return description;
}

void Location::setName(string name_text)
{
    name = name_text;
}

string Location::getName()
{
    return name;
}

bool Location::loadFromFile(string file_name)
{
    string path = "locations/" + file_name + ".loc";
    ifstream locFile (path.c_str());

    #ifdef DEBUG
        cout << "! Read file: " << path << "\n";
    #endif // DEBUG

    if( not locFile.is_open() )
    {
        cout << "Nie znaleziono pliku lokalizacji dla " << file_name << "\n";
        return false;
    }

    unsigned int number_of_fields = -1;

    //TODO: Load and parse file - make universal function to find TAG and return value

    cout << "Wczytano lokalizację \"" << file_name << "\"\n";
    cout << number_of_fields << " pól\n";
    cout << "Opis:" << description << "\n";

    return true;
}
