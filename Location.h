#ifndef LOCATION_H
#define LOCATION_H

#include "Field.h"
#include <vector>

class Location
{
    public:
        Location();
        virtual ~Location();

        Field*  respawn_position();

        void    setRespawn(Field* respawn_field);

        string  getName();
        void    setName(string name_text);

        string  getDescription();
        void    setDescription(string description_text);

        bool    loadFromFile(string file_name);

    protected:

    private:
        std::vector < Field * > fields;
        Field*                  respawn;
        string                  name;
        string                  description;
        unsigned int            visibility;
};

#endif // LOCATION_H
