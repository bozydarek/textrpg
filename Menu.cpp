#include "Menu.h"
#include "functionKit.h"

#include <iostream>
#include <assert.h>

Menu::Menu()
    :selected(0)
    ,title("Menu")
    ,subtitle("")
{
    setPrompt("->");
}

Menu::~Menu()
{
    //dtor
}

int Menu::add_option( std::string name )
{
    int position = (int)options.size();

    for( auto i: options )
        if( i == name )
            return -1;

    options.push_back(name);

    return position;
}

void Menu::show()
{
    ClearScreen();
    std::cout << StrConvert("\n" + title + "\r\n");
    if(subtitle != "")
    {
        std::cout << StrConvert( subtitle + "\r\n");
    }
    std::cout << "\n";


    unsigned int size = options.size();
    for( unsigned int i=0; i < size; ++i )
    {
        std::cout << ( i == selected ? prompt : space);
        std::cout << StrConvert(options[i]) << "\r\n";
    }
}

bool Menu::setSelected( unsigned int pos )
{
    if( pos >= options.size() )
    {
        return false;
    }

    selected = pos;
    return true;
}

void Menu::switchOptions( menuKeys way )
{
    assert( options.size() > 0 );

    switch( way )
    {
        case menuKeys::upKey:
            if( selected == 0 )
            {
                selected = (unsigned int)options.size() - 1;
            }
            else
            {
                --selected;
            }
            break;

        case menuKeys::downKey:
            ++selected;
            if( selected == (unsigned int)options.size() )
            {
                selected = 0;
            }
            break;

        default:
            //assert( false ); // something went wrong
            break;
    }
}

void Menu::setTitle( std::string title )
{
    this->title = title;
}

void Menu::setSubtitle( std::string subtitle )
{
    this->subtitle = subtitle;
}

void Menu::setPrompt( std::string prompt )
{
    this->prompt = prompt;

    space = "";
    for( unsigned int i=0; i<prompt.size(); ++i )
    {
        space+=" ";
    }
}
