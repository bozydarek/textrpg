#ifndef MENU_H
#define MENU_H

#include <string>
#include <vector>
#include "Enums.h"

class Menu
{
    public:
        Menu();
        virtual ~Menu();

        int     add_option( std::string name );
        void    show();
        bool    setSelected( unsigned int pos );
        void    switchOptions( menuKeys way );

        void    setTitle( std::string title );
        void    setSubtitle( std::string subtitle );
        void    setPrompt( std::string _prompt );

    protected:

    private:
        std::vector <std::string>   options;
        unsigned int                selected;
        std::string                 title;
        std::string                 subtitle;
        std::string                 prompt;
        std::string                 space;
};

#endif // MENU_H
