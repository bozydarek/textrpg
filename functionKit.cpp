#include "functionKit.h"

#include <assert.h>
#include <iostream>

//#define __linux__

/*
  _______ _             _____
 |__   __| |           / ____|
    | |  | |__   ___  | |  __  __ _ _ __ ___   ___
    | |  | '_ \ / _ \ | | |_ |/ _` | '_ ` _ \ / _ \
    | |  | | | |  __/ | |__| | (_| | | | | | |  __/
    |_|  |_| |_|\___|  \_____|\__,_|_| |_| |_|\___|
*/

std::string splash[] = {
        "  _______ _             _____                      ",
        " |__   __| |           / ____|                     ",
        "    | |  | |__   ___  | |  __  __ _ _ __ ___   ___ ",
        "    | |  | '_ \\ / _ \\ | | |_ |/ _` | '_ ` _ \\ / _ \\",
        "    | |  | | | |  __/ | |__| | (_| | | | | | |  __/",
        "    |_|  |_| |_|\\___|  \\_____|\\__,_|_| |_| |_|\\___|"
};
#define HORIZONTAL_OFFSET   13
#define HEIGHT_OFFSET       5

#ifdef __linux__

    #include <unistd.h>
    #include <term.h> // if fatal error instal libncurses-dev
    #include <stdio.h>
    #include <curses.h>

    void init()
    {
        initscr();
        clear();
        noecho();
        cbreak();
    }

    void refreshTerminal()
    {
        clrtoeol();
        refresh();
        endwin();
    }

    void ClearScreen()
    {
        if (!cur_term)
        {
            int result;
            setupterm( NULL, STDOUT_FILENO, &result );

            if (result <= 0)
                return;
        }

      putp( tigetstr( "clear" ) );
    }

    std::string StrConvert(std::string tmp)
    {
        return tmp;
    }

    menuKeys getArrowsFromInput()
    {
        int c = getch();

        if (c == 10 )
        {
            return menuKeys::enterKey;
        }

        if (c == '\033')  // if the first value is esc
        {
            getch(); // skip the [
            switch( getch() )
            { // the real value
                case 'A':
                    return menuKeys::upKey;

                case 'B':
                    return menuKeys::downKey;
            }
        }

        return menuKeys::unknownKey;
    }

    void splashScreen()
    {
        int offset = HORIZONTAL_OFFSET;
        int height = HEIGHT_OFFSET;

        for( auto i : splash)
        {
             mvprintw( height++, offset, i.c_str());
        }

        mvprintw( height + 4, offset, "  Press Enter to play or any other key to see menu.");

        mvprintw( 20, 5 , "version:");
        mvprintw( 20, 15 , VersionName );
    }

#elif _WIN32

    #include <conio.h>
    #include <stdlib.h>

    void init()
    {
    }

    void refreshTerminal()
    {
    }

    void ClearScreen()
    {
        //clrscr();
        system("CLS");
    }

    //Polish letter      Ą 	    Ć 	    Ę 	    Ł 	    Ń 	    Ó 	    Ś 	    Ź 	    Ż 	    ą 	    ć 	    ę 	    ł 	    ń 	    ó 	    ś 	    ź 	    ż
    //CP852 (Latin II) 	 164 	143 	168 	157 	227 	224 	151 	141 	189 	165 	134 	169 	136 	228 	162 	152 	171 	190
    //Latin II - OCT     244	217	    250	    235	    343	    340	    227	    215 	275 	245 	206 	251 	210 	344 	242 	230 	253 	276

    #define LETTERS 18

    std::string characters[LETTERS][2] = {  {"Ą", "\244"}, 	{"Ć", "\217"}, 	{"Ę", "\250"}, 	{"Ł", "\235"}, 	{"Ń", "\343"}, 	{"Ó", "\340"}, 	{"Ś", "\227"}, 	{"Ź", "\215"}, 	{"Ż", "\275"},
                                            {"ą", "\245"}, 	{"ć", "\206"}, 	{"ę", "\251"}, 	{"ł", "\210"}, 	{"ń", "\344"}, 	{"ó", "\242"}, 	{"ś", "\230"}, 	{"ź", "\253"}, 	{"ż", "\276"} };

    void replaceAll(std::string& str, const std::string& from, const std::string& to)
    {
        if(from.empty())
        {
            return;
        }

        size_t start_pos = 0;

        while( (start_pos = str.find(from, start_pos)) != std::string::npos )
        {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
        }
    }

    std::string StrConvert(std::string tmp)
    {
        for( int i = 0; i < LETTERS; ++i )
        {
            replaceAll( tmp, characters[i][0], characters[i][1]);
        }

        return tmp;
    }

    //72: Up arrow; 75: Left arrow; 77: Right arrow; 80: Down arrow;
    menuKeys getArrowsFromInput()
    {
        switch( getch() )
        {
            case 72:
                return menuKeys::upKey;

            case 80:
                return menuKeys::downKey;

            case 13:
                return menuKeys::enterKey;
        }

        return menuKeys::unknownKey;
    }


    void splashScreen()
    {
        std::string offset = "";

        for (int i = 0; i < HORIZONTAL_OFFSET; ++i)
        {
           offset += " ";
        }

        for (int i = 0; i < HEIGHT_OFFSET; ++i)
        {
           std::cout << "\n";
        }

        for( auto i : splash)
        {
             std::cout << offset << i.c_str() << "\n";
        }

        std::cout << "\n" << offset << "              Press any key to see menu.\n\n";

        for (int i = 0; i < HEIGHT_OFFSET; ++i)
        {
           std::cout << "\n";
        }

        std::cout << "     version: " << VersionName << "\n";

        getch();
    }
#else
    std::cout << "Nie wspierana wersja systemu operacyjnego!";
#endif



