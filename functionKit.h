#ifndef FUNCTION_KIT_H
#define FUNCTION_KIT_H

#include <string>
#include "Enums.h"

#define VersionName "Alpha 1.0 - Wakacyjna Sielanka"

    void        init();
    void        refreshTerminal();
    void        ClearScreen();
    std::string StrConvert( std::string tmp );
    menuKeys    getArrowsFromInput();
    void        splashScreen();

#endif // FUNCTION_KIT_H
