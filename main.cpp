#include <iostream>
#include <string>

#include "Location.h"
#include "functionKit.h"
#include "Menu.h"
#include "parsers.h"

using namespace std;

int main()
{
    init();
    // TODO: add to add_option, second variable - type menu or function()
    Menu mainMenu;
    mainMenu.setTitle("-> Mistyczny tytuł tej magicznej gry <-");
    mainMenu.add_option("Nowa gra");
    mainMenu.add_option("Kontynuuj");
    mainMenu.add_option("Opcje");
    mainMenu.add_option("Autorzy");
    mainMenu.add_option("Wyjście z gry");

    #if _WIN32
        mainMenu.setPrompt("\020");
    #endif // _WIN32

    splashScreen();

// TODO:
// close this in function
// trace bug with empty screen
//--------------------------
    bool ppp = true; // temporary variable
    while( ppp )
    {
        mainMenu.show();

        menuKeys input = getArrowsFromInput();

        if( input == menuKeys::enterKey )
        {
            ppp = false;
        }
        else if( input == menuKeys::unknownKey )
        {

        }
        else
        {
            mainMenu.switchOptions( input );
        }
    }

    refreshTerminal();
//--------------------------

    if( not loadDictionaries() )
    {
        return 1;
    }

    //TODO: Read location form /locations/[start.loc]

    // temporary location and fields
    Location start;


    // TODO: After done loadFromFile uncomment this and delete 74-77
    /*if( not start.loadFromFile("start") )
    {
        return 2;
    }*/

    Field * sp = new Field();
    sp->setDescription(StrConvert("Budzisz się w jakimś mrocznym pomieszczeniu."));

    start.setRespawn(sp);

    bool game = true;

    Field *position = start.respawn_position();
    string playerInput;

    // TODO move main game loop to game.cpp

    while( game )
    {
        ClearScreen();
        cout << "\n ~ " << StrConvert(position->getDescription()) << "\n";
        cout << "\n Co robisz? \n\n> ";

        bool getUserInput = true;

        while(getUserInput)
        {
            getline( std::cin, playerInput );
            getUserInput = false;
            // parse action
            switch( parseActions(playerInput) ) // and do something
            {
                case actions::fight:
                    break;

                case actions::inventory:
                    break;

                case actions::look:
                    break;

                case actions::step:
                    {
                        // TODO: Handle parseMove output
                        directions out = parseMove(playerInput);
                        #ifdef DEBUG
                            cout << "Move " << out << "\n";
                            getline( std::cin, playerInput );
                        #endif // DEBUG
                    }
                    break;

                case actions::take:
                    break;

                case actions::talk:
                    break;

                case actions::taunt:
                    break;

                case actions::use:
                    break;

                case actions::exitGame:
                    // TODO: Are you sure to exit the game? Do you want to save the game?
                    game = false;
                    break;

                case actions::unknown:
                default:
                    cout << "\n> ";
                    getUserInput = true;
            }
        }
    }

    return 0;
}
