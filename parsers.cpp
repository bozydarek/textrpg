#include "parsers.h"

#include <array>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <assert.h>

#include "functionKit.h"

using namespace std;
array< vector<string>, ACTIONS_COUNTER-1 > action_key_words;
array< vector<string>, 4 > move_key_words;

template< std::size_t N, std::size_t SIZE >
static bool loadDic( string name, pair< string, actions > (&files)[N], array< vector<string>, SIZE >& key_words_lists )
{
    for( auto i : files )
    {
        string path = "dictionary/" + name + "/"+ i.first;
        ifstream inFile(path.c_str());

        #ifdef DEBUG
            cout << "! Read file: " << path << "\n";
        #endif // DEBUG

        if( not inFile.is_open() )
        {
            cout << "Nie znaleziono pliku słownika dla " << name << "/" << i.first << "\n";
            return false;
        }

        string strPhrase;
        while( getline(inFile, strPhrase) )
        {
            size_t found = strPhrase.find('#');
            if( found != std::string::npos )
            {
               strPhrase.erase(strPhrase.begin() + found, strPhrase.end());
            }

            if( strPhrase == "" )
            {
                continue;
            }

            unsigned int offset=0;
            while(strPhrase[offset] == ' ')
            {
                ++offset;
            }

            if( offset != 0 )
            {
               strPhrase.erase(strPhrase.begin(), strPhrase.begin()+offset);
            }

            offset=strPhrase.length()-1;
            while(strPhrase[offset] == ' ')
            {
                --offset;
            }

            if( offset != strPhrase.length()-1 )
            {
               strPhrase.erase(strPhrase.begin()+offset+1, strPhrase.end());
            }

            if( strPhrase != "" )
            {
                #ifdef DEBUG
                    cout << strPhrase << "\n";
                #endif // DEBUG
                key_words_lists[ i.second - 1 ].push_back(StrConvert(strPhrase));
            }
        }

        inFile.close();
    }

    return true;
}

static bool loadActionDic()
{
    const size_t arraySize = 9;
    pair< string, actions > dictionaryFiles[arraySize] = {
        make_pair("step", step),
        make_pair("fight", fight),
        make_pair("use", use),
        make_pair("take", take),
        make_pair("look", look),
        make_pair("inventory", inventory),
        make_pair("taunt", taunt),
        make_pair("talk", talk),
        make_pair("exitGame", exitGame)
    };
    return loadDic<arraySize> ("actions", dictionaryFiles, action_key_words);
}

static bool loadMoveDic()
{
    const size_t arraySize = 4;
    pair< string, actions > dictionaryFiles[arraySize] = {
        make_pair("up", step),
        make_pair("right", fight),
        make_pair("left", use),
        make_pair("down", take)
    };
    return loadDic <arraySize> ("moves", dictionaryFiles, move_key_words);
}

bool loadDictionaries()
{
    return loadActionDic() & loadMoveDic(); // & etc ...
}

bool BothAreSpaces(char lhs, char rhs)
{
    return (lhs == rhs) && (lhs == ' ');
}
// TODO: Make universal function "parser" and change parseActions and parseMove to use it

actions parseActions( string text )
{
    transform(text.begin(), text.end(), text.begin(), ::tolower);

    string::iterator new_end = unique(text.begin(), text.end(), BothAreSpaces);
    text.erase(new_end, text.end());

    bool action_recognition = false;
    actions rec_act = actions::unknown;

    for(int i=1; i<actions::ACTIONS_COUNTER; ++i) // first (at 0 pos) is unknown
    {
        for( auto phrase: action_key_words[i-1] )
        {
            size_t found = text.find( phrase );

            if( found != string::npos )
            {
                if( not action_recognition || rec_act == (actions)i ) // second case is here because for example: "Run, sprint, dash, run for it!"
                {
                    action_recognition = true;
                    rec_act = (actions)i;
                }
                else
                {
                    return actions::unknown;
                }
            }
        }
    }

    return rec_act;
}

directions parseMove( string text )
{
    transform(text.begin(), text.end(), text.begin(), ::tolower);

    string::iterator new_end = unique(text.begin(), text.end(), BothAreSpaces);
    text.erase(new_end, text.end());

    bool direction_rec = false;
    directions rec_direct = directions::error;

    for(int i=0; i<4; ++i)
    {
        for( auto phrase: move_key_words[i] )
        {
            size_t found = text.find( phrase );

            if( found != string::npos )
            {
                if( not direction_rec || rec_direct == (directions)i ) // second case like in parseActions
                {
                    direction_rec = true;
                    rec_direct = (directions)i;
                }
                else
                {
                    return directions::error;
                }
            }
        }
    }

    return rec_direct;
}
