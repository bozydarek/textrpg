#ifndef PARSERS_H
#define PARSERS_H

#include <string>

#include "Enums.h"

    bool        loadDictionaries();
    actions     parseActions( std::string text );
    directions  parseMove( std::string text );

#endif // PARSERS_H


